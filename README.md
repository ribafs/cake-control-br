Plugin para Controle de Acesso com CakePHP 3
===========================================

# ALERTA
Este projeto foi Descontinuado.
Eu queria mudar o nome, mas como não quiz perder as referências, mantive este e criei um outro, que é o cake-acl-br:
https://github.com/ribafs/cake-acl-br
Daqui pra frente estarei trabalhando, atualizando, melhorando apenas o cake-acl-br.

[![Licença](https://img.shields.io/packagist/l/doctrine/orm.svg?maxAge=2592000)](https://github.com/ribafs/cake-control-br/blob/master/LICENSE)

URL deste projeto - https://github.com/ribafs/cake-control-br/

## ALERTA2
Com o lançamento da versão 3.6 do CakePHP o plugin cake-control-br deixou de funcionar.
Agora temos duas dependências:
- PHP 7.2 ou superior
- CakePHP 3.5.13 no máximo

Este plugin inclue o plugin BootstrapUI e o (Twitter) Bootstrap e também inclui a estrutura do plugin [twbs-cake-plugin](https://github.com/elboletaire/twbs-cake-plugin).

## Principais recursos
    Template do bake traduzido para pt_BR
    Element topmenu
    Busca com paginação
    Senhas criptografadas com Bcrypt
    Controle de Acesso Admin/Panel web    
    Dois Layouts: admin e default com o Bootstrap
    Utilitários do BootstrapUI plugin

## Instalação e uso
https://github.com/ribafs/cake-control-br

Criar app:

    composer create-project --prefer-dist cakephp/app control1

Instalar Plugin

    cd control1

    composer require ribafs/cake-control-br

Configurar banco

Crie o banco e importe o script existente na pasta docs do plugin baixado. Depois edite config/app.php para configurar o banco.

Aproveite e configure também o controller default em config/routes.php para um de seu interesse.

Habilitar o plugin

    bin/cake plugin load CakeControlBr --bootstrap

Download do plugin

    https://github.com/ribafs/cake-control-br/archive/master.zip

Descompactar e abrir o diretório docs, então copiar:

    bootstrap_cli.php para a control1/config (Com isso o Bake gerarará Users com login e logout)

    AppController.php para control1/src/Controller

    cd control1

    bin/cake bake all groups -t CakeControlBr

    bin/cake bake all users -t CakeControlBr

    bin/cake bake all permissions -t CakeControlBr

    bin/cake bake all customers -t CakeControlBr


Existem 4 usuários, cada um com permissões diferentes:

super - com senha super também tem total permissão em tudo.

admin - com senha admin tem total permissão nas tabelas groups, users e permissions.

manager - com senha manager tem total permissão somente nas tabelas diferentes de groups, users e permissions.

user - com senha user não tem nenhuma permissão no aplicativo, apenas de efetuar login.


Em AppController.php você pode definir o controller default para usuários não administradores. Caso não use a tabela customers troque logo no início do AppController por uma de suas tabelas na linha:

    protected $noAdmins = 'Customers';


# Documentação
Alguns detalhes a mais - https://ribafs.github.io/cakephp/cake-control.pdf


## Sugestões, colaborações e forks serão muto bem vindos:

- Erros: português
- PHP
- CakePHP
- ControlComponent.php
- etc

License
-------

The MIT License (MIT)
